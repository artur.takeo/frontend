const botaoCadastro = document.querySelector("#buttonCadastro");
const botaoLogin = document.querySelector("#buttonLogin");

//192.168.122.1
botaoLogin.addEventListener('click', () => {
    const inputUsuario = document.querySelector("#usuario").value;
    const inputSenha = document.querySelector("#senha").value;

    let loginObject = {
        email: inputUsuario,
        senha: inputSenha
    };

    fetch("http://138.68.237.126/login", {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
        body: JSON.stringify(loginObject)
    })  
    .then(function(res){ 
        console.log(res) 
    })    
    .catch(function(res){
        console.log(res) 
    })
});

botaoCadastro.addEventListener('click', () => {
    window.location = "cadastro.html";
});
